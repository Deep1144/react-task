import {AuthActions} from "../actions/auth_actions";

const initialState = {
    currentUser: {},
    isLoading : false,
    isLoggedIn: false
};

export const AuthReducer = (state = initialState, action) => {
    switch (action.type) {
        case AuthActions.LOGIN:
            localStorage.setItem('user',JSON.stringify(action.payload));
            return {
                ...state,
                isLoggedIn: true,
                currentUser: action.payload
            };
        case AuthActions.LOGIN_ERROR:
            return {
                ...state,
                isLoggedIn: false,
                currentUser: {}
            };
        case AuthActions.SIGNUP:
            return {
                ...state,
                isLoggedIn : false,
                currentUser :action.payload
            };
        case AuthActions.VERIFY_EMAIL:
            localStorage.setItem('user',JSON.stringify(action.payload));
            return {
                ...state,
                isLoggedIn :true,
                currentUser :action.payload
            };
        case AuthActions.LOGOUT:
            return {
                ...state,
              isLoggedIn :false,
              currentUser :{}
            };
        case AuthActions.UPDATE:
            localStorage.setItem('user',JSON.stringify(action.payload));
            return {
                ...state,
                currentUser : action.payload
            };
        case AuthActions.LOADING:
            return {
                ...state,
                isLoading :action.payload
            };
        default :
            return state;
    }
};