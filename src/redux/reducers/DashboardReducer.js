import {dashBoardActions} from "../actions/dashboard_actions";

const initialState = {
    newData: {
        companyId:'',
        name: '',
        dateOfJoin: new Date(),
        dateOfBirth: new Date(),
        contactNo:'',
        hobbies: [],
        profilePhoto: '',
        address: {
            city : '',
            state : '',
            countary : ''
        }
    },
    list: [],
    currentIndex : -1
};


export const DashBoardReducer = (state = initialState, action) => {
    // console.log(state)
    switch (action.type) {
        case dashBoardActions.FETCH:
            return {
                ...state,
                currentIndex : -1,
                list : action.payload
            };
        case dashBoardActions.INSERT:
            return {
                ...state ,
                list :[...state.list , action.payload]
            };
        case dashBoardActions.UPDATE_INDEX:
            return {
                ...state,
                currentIndex: action.payload
            };
        case dashBoardActions.DELETE:
            return {
                ...state,
                currentIndex : -1,
                list : action.payload.list
            };
        default :
            return state;
    }
};