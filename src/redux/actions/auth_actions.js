import axios from 'axios';

export const AuthActions = {
    LOGIN : 'LOGIN',
    SIGNUP : 'SIGNUP',
    LOGIN_ERROR : 'LOGIN_ERROR',
    VERIFY_EMAIL : 'VERIFY_EMAIL',
    LOGOUT : 'LOGOUT',
    UPDATE : 'UPDATE',
    LOADING : 'LOADING'
};

export const baseUrl = 'http://localhost:3000/company/';

export const Login = (data)=>{
    return (dispatch)=>{
        let loginData = {
            email : data.email,
            password : data.password
        };
        axios.post(baseUrl+'login' , loginData).then(
            response =>{
                dispatch({
                    type : AuthActions.LOGIN,
                    payload : response.data.data[0]
                });
            }
        ).catch(error=>{
            console.log("Error : ",error);
            dispatch({
                type: AuthActions.LOGIN_ERROR,
                payload: error
            })
        });
    }
};


export const Signup = (data)=>{
    return (dispatch)=>{
        dispatch(Loading(true));
        let signupData = {
            email : data.email,
            password : data.password,
            name : data.name
        };
        axios.post(baseUrl+'signup' , signupData).then(
            response =>{
                dispatch({
                    type : AuthActions.SIGNUP,
                    payload : response.data.data
                });
                dispatch(Loading(false));
            }
        ).catch(error=>{
            console.log("Error : ",error);
            dispatch({
                type: AuthActions.LOGIN_ERROR,
                payload: error
            })
        });
    }
};


export const verifyEmail = (id)=>{
    return (dispatch)=>{
        axios.post(baseUrl+'verifyEmail' , {id }).then(
            response=>{
                console.log(response);
                dispatch({
                    type:AuthActions.VERIFY_EMAIL,
                    payload : response.data.data
                })
            }
        ).catch(error=>{
            console.log(error);
        })
    }
};

export const UpdateCompany= (newData)=>{
  return (dispatch)=>{
      axios.post(baseUrl+'updateCompany',newData).then(response =>{
          dispatch({
              type : AuthActions.UPDATE,
              payload : response.data.data
          })
      })
  }
};

export const localStorageLogin= (data)=>{
    return {
        type : AuthActions.LOGIN,
        payload :data
    }
};

export const logout =()=>{
    return {
        type : AuthActions.LOGOUT
    }
};


export const Loading = (loadingState)=>{
    return {
        type:AuthActions.LOADING,
        payload : loadingState
    }
};