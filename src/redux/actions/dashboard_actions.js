import axios from 'axios';
import {AuthActions, UpdateCompany} from "./auth_actions";

export const dashBoardActions = {
    INSERT : 'INSERT',
    DELETE : 'DELETE',
    FETCH : 'FETCH',
    UPDATE_INDEX : 'UPDATE_INDEX'
};
const baseUrl = 'http://localhost:3000/employees/';
export const fetchEmployee = (id)=>{
    return (dispatch)=>{
        axios.get(baseUrl+'get-employee/'+id).then(
            response=>{
                dispatch({
                    type:dashBoardActions.FETCH,
                    payload : response.data.data
                })
            }
        )
    }
};


export const addEmployee = (data)=>{
    return (dispatch)=>{
        axios.post(baseUrl+'add-employee',data ).then(
            response=>{
               dispatch({
                   type : dashBoardActions.INSERT,
                   payload: response.data.data
               })
            }
        )
    }
};

export const deleteEmployee = (id,index )=>{
    return (dispatch,getState)=>{
        let state = getState();
        axios.delete(baseUrl+'delete-employee/'+id).then(response=>{
            dispatch(fetchEmployee(state.authReducer.currentUser._id))
        })
    }
};

export const updateIndex = (index)=>{
    return {
        type : dashBoardActions.UPDATE_INDEX,
        payload :index
    }
};


export const updateEmployee = (data)=>{
    return (dispatch,getState)=>{
        const state = getState();
        axios.post(baseUrl+'update-employee',data).then(response=>{
            dispatch(fetchEmployee(state.authReducer.currentUser._id));
        })
    }
};

export const uploadLogo = (formData)=>{
    return (dispatch,getState)=>{
        let state = getState().authReducer;
        axios.post('http://localhost:3000/company/logo',formData).then(response=>{
            dispatch(UpdateCompany({
                ...state.currentUser,
                logo : response.data.path
            }))
        })
    }
};