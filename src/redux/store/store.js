import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import reduxThunk from 'redux-thunk';
import {AuthReducer} from "../reducers/AuthReducer";
import {DashBoardReducer} from "../reducers/DashboardReducer";


export const rootReducer = combineReducers({
    authReducer : AuthReducer,
    dashBoardReducer : DashBoardReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(rootReducer, /* preloadedState, */ composeEnhancers(
    applyMiddleware(reduxThunk)
));

