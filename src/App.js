import React from 'react';
import './App.css';
import AuthPage from "./components/auth";
import {BrowserRouter, Redirect, Switch, } from 'react-router-dom';
import {Route} from "react-router-dom";
import DashBoard from "./components/DashBoard";
import {useDispatch, useSelector} from 'react-redux';
import VerifyEmail from "./components/VerifyEmail";
import {localStorageLogin} from "./redux/actions/auth_actions";

function App(props) {
    const isLoggedIn = useSelector(state => state.authReducer.isLoggedIn);
    const dispatch = useDispatch();

    let userString = localStorage.getItem('user');
    if(userString){
      let user = JSON.parse(userString);
      dispatch(localStorageLogin(user));
    }
    return (
        <div className="App">
            <BrowserRouter>
                <Switch>
                    <Route path='/auth' exact component={AuthPage}/>
                    <Route path='/verify-email/:id' exact component={VerifyEmail}/>
                    {isLoggedIn && <Route path='/dashboard' component={DashBoard}/>}
                    {!isLoggedIn && <Redirect  from='/' to='/auth'/>}
                    {isLoggedIn && <Redirect  from='/' to='/dashboard'/>}
                </Switch>
            </BrowserRouter>
        </div>
    );
}


export default App;
