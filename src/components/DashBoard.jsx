import {Component} from "react";
import React from "react";
import {connect} from "react-redux";
import {addEmployee, fetchEmployee, updateEmployee} from "../redux/actions/dashboard_actions";
import Card from "@material-ui/core/Card/Card";
import TextField from "@material-ui/core/TextField/TextField";
import {Checkbox, FormControlLabel, withStyles} from '@material-ui/core';
import Button from "@material-ui/core/Button/Button";
import List from "./List";
import {withRouter} from "react-router-dom";
import {logout} from "../redux/actions/auth_actions";
import NavBar from "./NavBar";


const styles = theme => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(3),
            width: '30ch',
        },
    },
    address : {
        '& .MuiTextField-root': {
            margin: theme.spacing(2),
            width: '19ch',
        },
    },
    margin: {
        margin: 20
    },
    mainDiv :{
        marginLeft : 75,
        marginRight : 75,
        marginTop : 20
    }
});


class DashBoard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: props.currentUser,
            newData: this.getStateObject(),
            currentIndex: props.currentIndex
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getStateObject = this.getStateObject.bind(this);
    }


    getStateObject() {
        if (this.props.currentIndex == -1) {
            return Object.assign({}, this.props.newData);
        } else {
            return Object.assign({}, this.props.list[this.props.currentIndex]);
        }
    }

    componentDidMount() {
        this.props.fetchEmployee(this.props.currentUser._id);
    }

    componentDidUpdate(oldProps) {
        if (oldProps.list.length !== this.props.list.length || oldProps.currentIndex !== this.props.currentIndex) {
            this.setState(state => {
                return {
                    ...state,
                    newData: this.getStateObject()
                }
            })
        }
    }

    handleInputChange(e) {
        let input = e.target;
        let type = input.type;
        console.log(type);
        let name = input.name;
        console.log(name)
        let value = type == 'checkbox' ? null : input.value;
        if (name === 'city' || name === 'state' || name === 'countary') {
            this.setState(state => {
                return {
                    ...state,
                    newData: {
                        ...state.newData,
                        address: {
                            ...state.newData.address,
                            [name]: value
                        }
                    }
                }
            })
        } else {
            this.setState((state) => {
                return {
                    ...state,
                    newData: {
                        ...state.newData,
                        [name]: value
                    }
                }
            });
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.props.currentIndex === -1) {
            this.state.newData = {
                ...this.state.newData,
                companyId: this.state.currentUser._id
            };
            console.log(this.state.newData);
            this.props.addEmployee(this.state.newData);
        } else {
            let obj = {
                ...this.props.list[this.props.currentIndex],
                ...this.state.newData
            };
            this.props.updateEmp(obj);
        }
    }

    CheckboxHandler = (e)=>{
        let name = e.target.name ;
        let isChecked = e.target.checked;
        let oldArr = [...this.state.newData.hobbies];
        if(isChecked){
            oldArr.push(name);
            console.log(oldArr) ;
            
        }else{
         oldArr = oldArr.filter(item => item !== name);
         console.log(oldArr)
        }
        this.setState(state => (
            {
                ...state,
                newData :{
                    ...state.newData,
                    hobbies : oldArr
                }
            }
        ));  
    }



    render() {
        const {classes} = this.props;
        const {newData} = this.state;
        return (
            <div>
                <NavBar/>
                <Card className={classes.mainDiv}>
                    <form className={classes.root} onSubmit={this.handleSubmit}>
                        <div>
                            <TextField type='text' label='name' name='name' value={newData.name}
                                       onChange={(e) => this.handleInputChange(e)}/>
                            <TextField label='contact no' type='text' name='contactNo'
                                       value={newData.contactNo} onChange={e => this.handleInputChange(e)}/>
                        </div>
                        <div>
                            <TextField id="date" label="Date of Birth" type="date" name='dateOfBirth'
                                       InputLabelProps={{
                                           shrink: true,
                                       }}
                                       selected={newData.dateOfBirth}
                                       onChange={e => this.handleInputChange(e)}
                            />
                            <TextField id="date" label="Date of Join" type="date" name='dateOfJoin'
                                       InputLabelProps={{
                                           shrink: true,
                                       }}
                                       onChange={e => this.handleInputChange(e)}
                            />
                        </div>
                        <div className={classes.address}>
                            <TextField type='text' label='city' name='city' value={newData.address.city}
                                       onChange={(e) => this.handleInputChange(e)}/>
                            <TextField type='text' label='state' name='state' value={newData.address.state}
                                       onChange={(e) => this.handleInputChange(e)}/>
                            <TextField type='text' label='countary' name='countary' value={newData.address.countary}
                                       onChange={(e) => this.handleInputChange(e)}/>
                        </div>

                        <div>
                            <FormControlLabel control={
                                <Checkbox color='primary' checked={newData.hobbies.includes('swimming')} onChange={(e)=>this.CheckboxHandler(e)} name='swimming'>swimming</Checkbox>
                            } label='swimming'/>
                           <FormControlLabel control={
                                <Checkbox color='primary' checked={newData.hobbies.includes('dancing')} onChange={(e)=>this.CheckboxHandler(e)} name='dancing'>dancing</Checkbox>
                            } label='dancing'/>
                            <FormControlLabel control={
                                <Checkbox color='primary' checked={newData.hobbies.includes('singing')} onChange={(e)=>this.CheckboxHandler(e)} name='singing'>singing</Checkbox>
                            } label='singing'/>
                        </div>

                        <div>
                            <Button className={classes.margin} color='primary' variant='contained'
                                    type='submit'>Submit</Button>
                        </div>
                    </form>
                </Card>
                <List/>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        currentUser: state.authReducer.currentUser,
        isLoggedIn: state.authReducer.isLoggedIn,
        newData: state.dashBoardReducer.newData,
        list: state.dashBoardReducer.list,
        currentIndex: state.dashBoardReducer.currentIndex,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        fetchEmployee: (id) => dispatch(fetchEmployee(id)),
        addEmployee: (data) => dispatch(addEmployee(data)),
        updateEmp: (data) => dispatch(updateEmployee(data)),
        Logout : ()=>dispatch(logout())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withStyles(styles)(DashBoard)));