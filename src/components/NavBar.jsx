import React,{Component,PureComponent} from "react";
import {logout} from "../redux/actions/auth_actions";
import {withRouter} from "react-router-dom";
import withStyles from "@material-ui/core/styles/withStyles";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import Typography from "@material-ui/core/Typography/Typography";
import Button from "@material-ui/core/Button/Button";
import connect from "react-redux/es/connect/connect";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import {uploadLogo} from "../redux/actions/dashboard_actions";
import avtar from './../assets/avtar-image.jpg';
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";

const styles = theme => ({
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        textAlign: 'left'
    },
    imageInDialog : {
        marginLeft : 70
    }
});


class NavBar extends PureComponent{
    state ={
        open : false
    };

    constructor(props){
        super(props);
        this.handleLogout = this.handleLogout.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleClickOpen = this.handleClickOpen.bind(this);
    }

    handleLogout(){
        localStorage.clear();
        this.props.Logout();
        this.props.history.replace('/');
    }
    handleClickOpen(){
        this.setState({
            open : true
        })
    }
    handleClose(){
        this.setState({
            open:false
        })
    }

    handleSubmit=()=>{
        let formData = new FormData();
        formData.append('logo' , this.state.selectedImage);
        this.props.UploadLogo(formData);
        this.setState({
            open:false,
            imgSrc : ''
        })
    };

    handleImageChange = (event)=>{
        var file = event.target.files[0];
        this.setState({
            selectedImage : file
        });
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);
        reader.onloadend = (e)=>{
            this.setState(state => ({
                ...state ,
                imgSrc : reader.result
            }))
        };
    };

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="h6" className={classes.title}>
                            {this.props.currentUser.name}
                        </Typography>
                        <img accept="image" onClick={this.handleClickOpen} src={this.props.currentUser.logo ? this.props.currentUser.logo: avtar } height='50' width='50'  />
                        <Button onClick={this.handleLogout} color="inherit">Logout</Button>
                    </Toolbar>
                </AppBar>

                <div>
                    <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                        <DialogTitle id="form-dialog-title" style={{width : '250px'}}>{this.props.currentUser.name}</DialogTitle>
                        <DialogContent>
                            <input hidden type='file' ref={(ref)=>this.inputFile = ref} onChange={this.handleImageChange}/>
                            <Button  style={{marginBottom : 50}}  onClick={()=>this.inputFile.click()}>Select</Button>
                            {
                                this.state.imgSrc && <img className={classes.imageInDialog} src={this.state.imgSrc} height='70' width='70' />
                            }
                            {
                                (this.props.currentUser.logo && !this.state.imgSrc) && <img className={classes.imageInDialog} src={this.props.currentUser.logo} height='70' width='70' />
                            }
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                Cancel
                            </Button>
                            <Button disabled={!this.state.imgSrc} onClick={this.handleSubmit} color="primary">
                                UPDATE
                            </Button>
                        </DialogActions>
                    </Dialog>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.authReducer.currentUser,
        isLoggedIn: state.authReducer.isLoggedIn,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        Logout : ()=>dispatch(logout()),
        UploadLogo : (formData)=> dispatch(uploadLogo(formData))
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(withRouter(withStyles(styles)(React.memo(NavBar))));