import React, {Component, useEffect, useState} from "react";
import {withRouter} from "react-router-dom";
import {useDispatch,useSelector} from 'react-redux';
import {verifyEmail} from "../redux/actions/auth_actions";
function VerifyEmail(props) {

    const dispatch = useDispatch();
    const user = useSelector(state => state.authReducer);

    useEffect(()=>{
        //http://localhost:3001/verify-email/5f5f28611cd3422808988637 demo url
        let id = props.match.params.id;
        dispatch(verifyEmail(id));
        if(user.isLoggedIn){
            props.history.replace('/dashboard');
        }
    },[user]);

    return (
        <>
            Verify
        </>
    )
}
export default withRouter(VerifyEmail);