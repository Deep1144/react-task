import React, {useEffect} from "react";
import {useSelector,useDispatch} from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {deleteEmployee, updateIndex} from "../redux/actions/dashboard_actions";
import Button from "@material-ui/core/Button/Button";
import * as moment from 'moment';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
    margin : {
        margin : 50
    }
});

function List() {
    const state = useSelector(state=>state);
    const list = useSelector(state=>state.dashBoardReducer.list);
    const dispatch = useDispatch();
    const classes = useStyles();

    useEffect(()=>{
    } ,[list]);

    function deleteEmp(id,index) {
        if(window.confirm('Are you sure')) {
            dispatch(deleteEmployee(id, index));
        }
    }

    function updateEmp(id,index){
        dispatch(updateIndex(index));
    }


    return(
        <div className={classes.margin}>
            <TableContainer >
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell align="right">City</TableCell>
                            <TableCell align="right">State</TableCell>
                            <TableCell align="right">Country</TableCell>
                            <TableCell align="right">Contact No</TableCell>
                            <TableCell align="right">Hobbies</TableCell>
                            <TableCell align="right">Date of Birth</TableCell>
                            <TableCell align="right">Date of Join</TableCell>
                            <TableCell align="right">Delete</TableCell>
                            <TableCell align='right'>Update</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            list.map((item,index)=>(
                                <TableRow key={item._id}>
                                    <TableCell>{item.name}</TableCell>
                                    <TableCell align="right">{item.address.city}</TableCell>
                                    <TableCell align="right">{item.address.state}</TableCell>
                                    <TableCell align="right">{item.address.countary}</TableCell>
                                    <TableCell align="right">{item.contactNo}</TableCell>
                                    <TableCell align="right">{item.hobbies.join(', ')}</TableCell>
                                    <TableCell align="right">{moment(new Date(item.dateOfBirth)).format( 'DD-MM-YYYY')}</TableCell>
                                    <TableCell align="right">{moment(new Date(item.dateOfJoin)).format( 'DD-MM-YYYY')}</TableCell>
                                    <TableCell align='right'>
                                        <Button variant="contained" color="secondary" onClick={()=>deleteEmp(item._id ,index)}>Delete</Button>
                                    </TableCell>
                                    <TableCell align='right'>
                                        <Button variant="contained" color="primary" onClick={()=>updateEmp(item._id ,index)}>Update</Button>
                                    </TableCell>
                                </TableRow>
                            ))
                        }
                    </TableBody>
                </Table>
            </TableContainer>
        </div>

    )
}

export default React.memo(List);