import React, {useEffect, useState} from "react";
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import {makeStyles} from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import Button from "@material-ui/core/Button/Button";
import {useDispatch, useSelector} from 'react-redux';
import {Login, Signup} from "../redux/actions/auth_actions";
import {withRouter} from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";

const useStyle = makeStyles({
    root: {
        width: '100%',
        margin: 'auto',
        '& > *': {
            margin: 20,
        },
    },
    cardContentMargin: {
        marginTop: 20,
        marginBottom: 5
    },
    cardWidth: {
        width: 300,
        margin: 'auto',
        marginTop: 25
    }
});

function AuthPage(props) {
    const classes = useStyle();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [name, setName] = useState('');
    const dispatch = useDispatch();
    const user = useSelector(state => state.authReducer);
    const [isLoginForm, setIsLoginForm] = useState(false);

    function handleLogin() {
        dispatch(Login({
            email: email,
            password: password,
        }));
    }

    function handleSignup() {
        dispatch(Signup({
            email: email,
            password: password,
            name: name
        }));
    }

    function handleSubmit(e) {
        e.preventDefault();
        isLoginForm ? handleLogin() : handleSignup();
    }

    useEffect(() => {
        if (user.isLoggedIn) {
            props.history.replace('/dashboard', user);
        }
    }, [user]);

    return (
        <div className={classes.root}>
            <Card className={classes.cardWidth}>
                <CardContent className={classes.cardContentMargin}>
                    {user.isLoading && <CircularProgress />}

                    <form onSubmit={(e) => handleSubmit(e)}>
                        <TextField label='Email' onChange={(e) => setEmail(e.target.value)} type='text'/>
                        <TextField label='Password' onChange={(e) => setPassword(e.target.value)} type='password'/>
                        {
                            isLoginForm &&
                            <>
                                <Button type='submit' style={{marginTop: 25}} variant="contained" color="primary">
                                    Login
                                </Button>
                            </>
                        }
                        {
                            !isLoginForm &&
                            <div>
                                <TextField error={password !== confirmPassword} label='Confirm Password'
                                            onChange={(e) => setConfirmPassword(e.target.value)}
                                            type='password'/>

                                <TextField label='Company Name' onChange={(e) => setName(e.target.value)}/>
                                <Button type='submit' style={{marginTop: 25}} variant="contained" color="primary">
                                    Signup
                                </Button>
                            </div>
                        }
                    </form>

                    <div style={{marginTop: 20}}>
                        <Button color='primary' onClick={() => {
                            setIsLoginForm(true)
                        }}>Login</Button>
                        /
                        <Button color='secondary' onClick={() => {
                            setIsLoginForm(false)
                        }}>Signup</Button>
                    </div>
                </CardContent>
            </Card>
        </div>
    )
}

export default withRouter(AuthPage);