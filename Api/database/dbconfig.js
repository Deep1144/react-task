const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/mean', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(res => { console.log("connected to database : ") })
    .catch(err => { console.log("Error while connecting database : " + err) });