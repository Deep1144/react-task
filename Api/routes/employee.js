const express = require('express');
const { addEmployee, getEmployee, updateEmployee, deleteEmployee } = require('../controllers/employee.controller');
const router = express.Router();
const multer = require('multer');

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads')
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + file.originalname);
    }
})

var upload = multer({ storage: storage })

router.post('/add-employee', addEmployee);
router.post('/update-employee', updateEmployee);
router.get('/get-employee/:id', getEmployee);
router.delete('/delete-employee/:id', deleteEmployee);
router.post('/upload-image', upload.single('image'), (req, res, next) => {
    // console.log(req.file);
    res.json({ error: false, path: 'http://localhost:3000/' + req.file.path });
})
module.exports = router;