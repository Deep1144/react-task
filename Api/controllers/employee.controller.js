const employeeModel = require("../models/employee.model");

const addEmployee = (req, res, next) => {
    delete req.body._id;
    console.log(req.body)
    let emp = new employeeModel(req.body);
    emp.save((err, doc) => {
        if (err) {
            res.status(400).json({ error: true, message: "Not added", data: err });
        } else {
            res.status(201).json({ error: false, message: "Employee Added", data: doc });
        }
    })
}


const getEmployee = (req, res, next) => {
    let companyId = req.params.id;
    employeeModel.find({ companyId: companyId }, (err, docs) => {
        if (err) {
            res.status(400).json({ error: true, message: "Not Found", data: err });
        } else {
            res.status(201).json({ error: false, data: docs });
        }
    })
}

const updateEmployee = (req, res, next) => {
    let empid = req.body._id;
    console.log(empid)
    employeeModel.findByIdAndUpdate({ _id: empid }, req.body).then(result => {
        // console.log(result);
        res.status(200).json({ error: false, data: result });
    }).catch((err) => {
        console.log(err);
        res.status(400).json({ error: true, message: "Not Found", data: err });
    })
}


const deleteEmployee = (req, res, next) => {
    let empId = req.params.id;
    employeeModel.findByIdAndRemove(empId).then(result => {
        // console.log(result);
        res.status(200).json({ error: false, data: result });
    }).catch(err => {
        console.log(err);
        res.status(400).json({ error: true, message: "Not Found", data: err });
    })
}
module.exports = {
    addEmployee,
    getEmployee,
    updateEmployee,
    deleteEmployee
}