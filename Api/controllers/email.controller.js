var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    requireTLS: true,

    auth: {
        user: 'deep.webosmotic@gmail.com',
        pass: 'Deep@123'
    }
});

function sendAuthMail(mail, generatedLink) {
    var mailOptions = {
        from: 'deep.webosmotic@gmail.com',
        to: mail,
        subject: 'Please verify your account',
        html: `<h1>Click link given below to verify</h1><br><a href='${generatedLink}'>Verify</a>`
    };
    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log(error);
                reject(error);
            } else {
                // console.log('Email sent: ' + info.response);
                resolve();
            }
        });
        console.log(generatedLink);
        // resolve();
    })
}
module.exports = sendAuthMail;