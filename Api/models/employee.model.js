const mongoose = require("mongoose");

const employeeSchema = new mongoose.Schema({
    companyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'companyModel'
    },
    name: {
        type: String,
        required: true,
    },
    dateOfJoin: {
        type: Date,
    },
    dateOfBirth: {
        type: Date,
    },
    profilePhoto: {
        type: String,
    },
    contactNo: {
        type: String,
    },
    address: {
        city: String,
        state: String,
        countary: String,
    },
    hobbies: Array,
}, { collection: "employee" }, { timestamps: true });

const employeeModel = mongoose.model('employeeModel', employeeSchema);

module.exports = employeeModel;