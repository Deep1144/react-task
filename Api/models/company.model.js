const mongoose = require('mongoose');

const companySchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true,
    },
    isEmailVerified: {
        type: Boolean,
        default: false
    },
    logo: {
        type: String,
        default: ''
    }
}, {
    collection: 'company'
}, {
    timestamps: true
});

const companyModel = mongoose.model('companyModel', companySchema);
module.exports = {
    companyModel
}